<?php

require('actions/database.php');

// Validation du formulaire
if(isset($_POST['validate'])){

    // Vérifier si l'utilisateur a bien complété tous les champs requis
    if(!empty($_POST['mail']) AND !empty($_POST['pseudo']) AND !empty($_POST['password'])){

        // Les données de l'utilisateur
        $user_mail = htmlspecialchars($_POST['mail']);
        $user_pseudo = htmlspecialchars($_POST['pseudo']);
        $user_password = $_POST['password'];

        // Définition de la fonction passwordIsValid
        function passwordIsValid($password) {
            if (preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,}$/', $password)) {
            return true;
            } else {
            return false;
            }
        }
        // Vérifier si le mot de passe est valide
        if (passwordIsValid($user_password)) {
            // Si oui, hasher le mot de passe
            $user_password = password_hash($user_password, PASSWORD_DEFAULT);

            // Vérifier si l'utilisateur existe déjà sur le site
            $checkIfUserAlreadyExists = $bdd->prepare('SELECT pseudo FROM users WHERE pseudo = ?');
            $checkIfUserAlreadyExists->execute(array($user_pseudo));

            if($checkIfUserAlreadyExists->rowCount() == 0){

                // Insérer l'utilisateur dans la bdd
                $insertUserOnWebsite = $bdd->prepare('INSERT INTO users(mail, pseudo, password)VALUES(?,?,?)');
                $insertUserOnWebsite->execute(array($user_mail, $user_pseudo, $user_password));

                // récupérer les informations de l'utilisateur
                $getInfosOfThisUserReq = $bdd->prepare('SELECT id, mail, pseudo FROM users WHERE mail = ? AND pseudo = ?');
                $getInfosOfThisUserReq->execute(array($user_mail, $user_pseudo));

                $userInfos = $getInfosOfThisUserReq->fetch();

                //Authentifier l'utilisateur sur le site et récupérer ses données dans des variables sessions
                $_SESSION['auth'] = true;
                $_SESSION['id'] = $userInfos['id'];
                $_SESSION['mail'] = $userInfos['mail'];
                $_SESSION['pseudo'] = $userInfos['pseudo'];

                // Rediriger l'utilisateur vers la page d'accueil
                header('Location: ./index.php');

            }else{
                $errorMsg = "L'utilisateur existe déjà sur le site";
            }
        } else {
            $errorMsg = "Le mot de passe n'est pas valide, veuillez respecter les critères de sécurité.";
        }
    }else{
        $errorMsg = "Veuillez compléter tout les champs...";
    }
}
