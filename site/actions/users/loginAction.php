<?php
session_start();
require('actions/database.php');

// Validation du formulaire
if(isset($_POST['validate'])){

    // Vérifier si l'utilisateur a bien complété tous les champs requis
    if(!empty($_POST['pseudo']) AND !empty($_POST['password'])){

        // Les données de l'utilisateur
        $user_pseudo = htmlspecialchars($_POST['pseudo']);
        $user_password = htmlspecialchars($_POST['password']);


        // Vérifier si l'utilisateur existe (si le pseudo est correct)
        $checkIfUserExists = $bdd->prepare('SELECT * FROM users WHERE pseudo = ?');
        $checkIfUserExists->execute(array($user_pseudo));

        if($checkIfUserExists->rowCount() > 0){

            // Récupérer les données de l'utilisateur
            $usersInfos =  $checkIfUserExists->fetch();

            // Vérifier si le mot de passe est correct
            if (password_verify($user_password, $usersInfos['password'])){

                //Authentifier l'utilisateur sur le site et récupérer ses données dans des variables sessions
                $_SESSION['auth'] = true;
                $_SESSION['id'] = $usersInfos['id'];
                $_SESSION['mail'] = $usersInfos['mail'];
                $_SESSION['pseudo'] = $usersInfos['pseudo'];

                // Vérifier si l'utilisateur est un administrateur
                if($usersInfos['is_admin'] == 1){
                    // Rediriger l'utilisateur vers la page d'administration
                    header('Location:admin/index.php');
                }else{
                    // Rediriger l'utilisateur vers la page d'accueil
                    header('Location:./index.php');
                }
            }else{
                $errorMsg = "Votre mot de passe est incorrect...";
            }
        }else{
            $errorMsg = "Votre pseudo est incorrect...";
        }
    }else{
        $errorMsg = "Veuillez compléter tout les champs...";
    }
}
?>
