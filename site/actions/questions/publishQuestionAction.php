<?php

require('actions/database.php');

if(isset($_POST['validate'])){

   if(!empty($_POST['title']) AND !empty($_POST['description']) AND !empty($_POST['content'])){

       $question_title = nl2br(htmlspecialchars($_POST['title']));
       $question_description = nl2br(htmlspecialchars($_POST['description']));
       $question_content = nl2br(htmlspecialchars($_POST['content']));
       $question_date = date('d/m/Y');
       $question_id_auteur = $_SESSION['id'];
       $question_pseudo_auteur = $_SESSION['pseudo'];

       $insertQuestionOnWebsite = $bdd->prepare('INSERT INTO questions(title, description, contenu, id_auteur, pseudo_auteur, date_publication)VALUES(?, ?, ?, ?, ?, ?)');
       $insertQuestionOnWebsite->execute(
         array(
            $question_title, 
            $question_description, 
            $question_content, 
            $question_id_auteur, 
            $question_pseudo_auteur, 
            $question_date
         )
      );


      $succesMsg = "Votre question a bien été publiée sur le site";

   }else{
       
      $errorMsg = "Veuillez compléter tout les champs...";
   }

}

