<?php 
require('actions/users/securityAction.php');
require('actions/questions/publishQuestionAction.php');

?>

<!DOCTYPE html>
<html lang="en">
<?php include 'includes/head.php'; ?>
<body>
<?php include('includes/navbar.php') ?>
<br><br>

<!-- Formulaire -->
<form class="container d-flex flex-column justify-content-center" method="POST">

<!-- Message d'erreur -->
<?php if(isset($errorMsg)){
      echo '<p>'.$errorMsg.'</p>';
      // Message Succes
    }elseif(isset($succesMsg)){
      echo '<p>'.$succesMsg.'</p>';
    }
?>

<!-- ADRESSE MAIL -->
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Titre de la question</label>
    <input type="text" class="form-control" name="title">
  </div>
  <!-- PSEUDO -->
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Description de la question</label>
    <textarea class="form-control" name="description"></textarea>
  </div>
  <!-- MDP -->
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Contenu de la question</label>
    <textarea class="form-control" name="content"></textarea>
  </div>
  <!-- BUTTON S'INSCRIRE -->
  <button type="submit" class="btn btn-primary" name="validate">Publier la question</button>
  <br><br>
</form>


</body>
</html>