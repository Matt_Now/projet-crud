<?php require('actions/users/signupAction.php'); ?>

<!DOCTYPE html>
<html lang="en">

<?php include './includes/head.php'; ?>

<body>
    
<br><br>

<!-- Formulaire -->
<form class="container" method="POST">

<!-- Message d'erreur -->
<?php if(isset($errorMsg)){echo '<p>'.$errorMsg.'</p>'; } ?>
<!-- ADRESSE MAIL -->
  <div class="mb-3">
  <label for="exampleInputEmail1" class="form-label">Adresse-mail</label>
    <input type="email" class="form-control" name="mail">
  </div>
  <!-- PSEUDO -->
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Pseudo</label>
    <input type="text" class="form-control" name="pseudo">
  </div>
  <!-- MDP -->
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Mot de passe</label>
    <input type="password" class="form-control" name="password">
  </div>
  <!-- BUTTON S'INSCRIRE -->
  <button type="submit" class="btn btn-primary" name="validate">S'inscrire</button>
  <br><br>
  <a href="./login.php" class="Deja-compte"><p>J'ai déjà un compte , se connecter.</p></a>
</form>


</body>
</html>